# LAVA Monitor

Software to monitor and produce metrics for [Prometheus](https://prometheus.io)

## Building

The LAVA Monitor is written in [Rust](https://www.rust-lang.org) and requires Cargo to be built.

Building lava-monitor is as simple as `cargo build`.

The project is using the [lava-api](https://gitlab.collabora.com/lava/lava-api) directly.

## Running

You can start directly lava-monitor with `cargo run -- https://lava.collabora.com`
or run it from the binary folder.

The LAVA monitor can be configured directly from the starting command, it also
supports environment variables:
 * `MATTERMOST_HOOK`: The Mattermost notification hook URL
 * `MATTERMOST_INTERVAL`: The time in seconds between each device poll
 * `LAVA_TOKEN`: The LAVA token required to retreive access-limited information
 * `LAVA_URL`: The LAVA URL, when set it is not required anymore to specify it as first argument

## Testing

You can get the metrics result from the LAVA monitor by getting `http://127.0.0.1:8884/metrics`.

## Batteries Included

A docker compose file is included to test it directly from end to end.
Simply use `docker-compose up` to bring the LAVA monitor, Prometheus and Grafana
containers up.

You can then Access Grafana from: http://127.0.0.1:3000

Prometheus status can be watched from: http://127.0.0.1:9090
